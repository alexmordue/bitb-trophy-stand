![BitB Trophy](trophy.jpg)

# Introduction

The trophy, when powered, will show up as an open WiFi access point. Connecting to it with a phone or laptop will send you to a captive portal (normally this functionality is used for connecting to public wifi access points that require you to register or accept terms and conditions, I think this is a more interesting use of the functionality). The portal page allows you to change the colour of the leds, or to enable/disable party mode :)

# Parts

You'll need:

* [3D printed stand](https://a360.co/2Quzm2w)
* Laser cut acrylic trophy (8mm thick)
* Wemos D1 Mini ESP8266 board
* 7 x SK6812 RGBW leds on ~10mm round PCBs
* Micro USB cable and power supply
* Some ribbon cable
* Soldering supplies
* 5 x short (6mm) m2 screws

# Firmware

In [trophy-firmware](trophy-firmware/) you'll find a PlatformIO project, this will compile to a hex file that can be uploaded, you'll also need to upload the contents of the data folder to SPIFFS storage on the esp8266, PlatformIO will be able to help you out with that.